FROM ruby:2.4-alpine3.6

RUN apk add --no-cache build-base git nodejs-npm ruby-dev tzdata

CMD [ "/bin/ash" ]
